import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {routeNames} from '../constants';
import SignInScreen from '../screnes/SignIn';
import HomeScreen from '../screnes/Home';
import ForecastScreen from '../screnes/Forecast';

const Stack = createStackNavigator();

const Router = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name={routeNames.signIn} component={SignInScreen} />
        <Stack.Screen name={routeNames.home} component={HomeScreen} />
        <Stack.Screen name={routeNames.forecast} component={ForecastScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Router;
