import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';
import {gradients} from '../../constants';
import {useSelector} from 'react-redux';
import {useDispatch} from 'react-redux';
import {switchGradient} from '../../store/app/actions';

const GradientButton = ({colors, onPress}) => {
  const {
    app: {currentGradient},
  } = useSelector(state => state);
  const isActive = currentGradient[0] === colors[0];
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={1}
      style={isActive ? styles.activeButtonContainer : styles.buttonContainer}>
      <LinearGradient style={styles.button} colors={colors} />
    </TouchableOpacity>
  );
};

const GradientSwitcher = () => {
  const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      {gradients.map((colors, key) => (
        <GradientButton
          key={key}
          colors={colors}
          onPress={() => dispatch(switchGradient(colors))}
        />
      ))}
    </View>
  );
};

export default GradientSwitcher;
