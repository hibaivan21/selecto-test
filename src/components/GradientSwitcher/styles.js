import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    height: 50,
    width: '100%',
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  activeButtonContainer: {
    height: 35,
    width: 35,
    borderRadius: 50,
  },
  buttonContainer: {
    height: 30,
    width: 30,
    borderRadius: 50,
  },
  button: {
    flex: 1,
    borderRadius: 50,
  },
});
