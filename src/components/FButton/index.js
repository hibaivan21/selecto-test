import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styles from './styles';

const FButton = ({onPress, title}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Text style={styles.title}>{title}</Text>
    </TouchableOpacity>
  );
};

export default FButton;
