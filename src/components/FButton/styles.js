import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    height: 40,
    backgroundColor: '#38559D',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
    marginHorizontal: 25,
  },
  title: {
    color: '#fff',
    fontWeight: '600',
  },
});
