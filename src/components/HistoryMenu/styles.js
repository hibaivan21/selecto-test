import {StyleSheet} from 'react-native';
import {isIphoneX} from 'react-native-iphone-x-helper';
import {dimensions} from '../../constants';

export default StyleSheet.create({
  container: {
    width: dimensions.DEVICE_WIDTH - 40,
    height: dimensions.DEVICE_HEIGHT,
    backgroundColor: 'white',
    padding: 20,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: isIphoneX() ? 30 : 0,
  },
  titleText: {
    fontSize: 22,
    fontWeight: '600',
  },
  spacer: {
    width: 40,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  nameText: {
    fontSize: 19,
  },
  emailText: {
    color: 'gray',
  },
});
