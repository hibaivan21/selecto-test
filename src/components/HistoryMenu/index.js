import React from 'react';
import {Alert, FlatList, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import HistoryItem from '../HistoryItem';
import {useNavigation} from '@react-navigation/native';
import ImageButton from '../ImageButton';
import styles from './styles';
import SignoutIcon from '../../assets/logout.png';
import {signOut} from '../../store/app/actions';
import {routeNames} from '../../constants';

const HistoryMenu = () => {
  const {
    app: {history},
    user: {email, name},
  } = useSelector(state => state);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const renderHistory = () => (
    <FlatList
      data={history}
      renderItem={({item}) => <HistoryItem item={item} />}
      numColumns={2}
      keyExtractor={(_, index) => index.toString()}
      showsVerticalScrollIndicator={false}
    />
  );

  const onPressSignOut = () => {
    Alert.alert('Log Out', 'Do you want to logout?', [
      {
        text: 'Yes',
        onPress: () => logOut(),
      },
      {
        text: 'No',
        onPress: () => {},
      },
    ]);
  };

  const logOut = () => {
    dispatch(signOut());
    navigation.replace(routeNames.signIn);
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.titleText}>History</Text>
      </View>
      {renderHistory()}
      <View style={styles.footer}>
        <View>
          <Text style={styles.nameText}>{name}</Text>
          <Text style={styles.emailText}>{email}</Text>
        </View>
        <ImageButton image={SignoutIcon} onPress={onPressSignOut} />
      </View>
    </View>
  );
};

export default HistoryMenu;
