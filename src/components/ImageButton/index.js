import React from 'react';
import {Image, TouchableOpacity} from 'react-native';

const ImageButton = ({image, onPress, width, height, style}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Image
        style={style}
        height={height || 25}
        width={width || 25}
        source={image}
        resizeMode="contain"
      />
    </TouchableOpacity>
  );
};

export default ImageButton;
