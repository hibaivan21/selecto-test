import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {useDispatch} from 'react-redux';
import {searchForecast, switchGradient} from '../../store/app/actions';
import styles from './styles';

const HistoryItem = ({item}) => {
  const {colors, city, date} = item;
  const dispatch = useDispatch();

  const onPressCity = value => {
    dispatch(searchForecast(value));
    dispatch(switchGradient(colors));
  };

  return (
    <TouchableOpacity
      onPress={() => onPressCity(city)}
      style={styles.container}>
      <LinearGradient style={styles.gradient} colors={colors}>
        <Text style={styles.cityText}>{city}</Text>
      </LinearGradient>
      <View style={styles.dateContainer}>
        <Text>{date.replaceAll('-', '/')}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default HistoryItem;
