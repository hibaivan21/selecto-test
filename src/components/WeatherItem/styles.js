import {StyleSheet} from 'react-native';
import {dimensions} from '../../constants';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  forecastContainer: {
    height: dimensions.DEVICE_WIDTH - 50,
    backgroundColor: '#fff',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  degreesText: {
    fontSize: 80,
  },
  dayText: {
    padding: 2,
    paddingHorizontal: 8,
    backgroundColor: '#fff',
  },
  dateText: {
    fontSize: 22,
    color: '#fff',
    alignSelf: 'center',
    marginTop: 50,
  },
});
