import React, {useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import styles from './styles';

const WeatherItem = ({item: {day, night, date}}) => {
  const [isDay, setIsDay] = useState(true);
  const backgroundColor = isDay ? '#fff' : '#0C2D48';
  const color = isDay ? '#0C2D48' : '#fff';

  return (
    <View style={styles.container}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => setIsDay(!isDay)}
        style={[styles.forecastContainer, {backgroundColor}]}>
        <Text style={[styles.degreesText, {color}]}>
          {Math.round(isDay ? day : night)}°C
        </Text>
        <Text style={styles.dayText}>{isDay ? 'DAY' : 'NIGHT'}</Text>
      </TouchableOpacity>
      <Text style={styles.dateText}>{date.replaceAll('-', '/')}</Text>
    </View>
  );
};

export default WeatherItem;
