import React from 'react';
import {KeyboardAvoidingView} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {useSelector} from 'react-redux';
import styles from './styles';

const Screen = ({children, style}) => {
  const {
    app: {currentGradient},
  } = useSelector(state => state);
  return (
    <KeyboardAvoidingView style={styles.container} behavior="height">
      <LinearGradient
        style={[styles.container, style]}
        colors={currentGradient}>
        {children}
      </LinearGradient>
    </KeyboardAvoidingView>
  );
};

export default Screen;
