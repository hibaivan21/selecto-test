import React from 'react';
import {TextInput} from 'react-native';
import styles from './styles';

const Input = ({
  style,
  value,
  onChangeText,
  placeholder,
  maxLength,
  textStyle,
}) => (
  <TextInput
    autoFocus
    value={value}
    onChangeText={onChangeText}
    style={[style, textStyle]}
    maxLength={maxLength}
    autoCapitalize={'none'}
    placeholder={placeholder}
  />
);

export default Input;
