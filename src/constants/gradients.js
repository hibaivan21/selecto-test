export default [
  ['#FF6969', '#AE2176'],
  ['#DD69FF', '#7A21AE'],
  ['#3ECC8D', '#3E88CC'],
  ['#FF69EA', '#AE2176'],
  ['#FFB469', '#AE3F21'],
  ['#C2289C', '#6065E3', '#6DD5FC'],
];
