const getDate = date => date.split(' ')[0];

export const forecastResultParser = list => {
  const forecast = list
    .map(item => {
      const {
        dt_txt,
        main: {temp},
      } = item;
      if (dt_txt.includes('15:00:00')) {
        return {date: getDate(dt_txt), day: temp};
      }
    })
    .filter(item => item !== undefined);

  list
    .map(item => {
      const {
        dt_txt,
        main: {temp},
      } = item;
      if (dt_txt.includes('03:00:00')) {
        forecast.find(item => item.date === getDate(dt_txt)).night = temp;
      }
    })
    .filter(item => item !== undefined);
  return forecast;
};
