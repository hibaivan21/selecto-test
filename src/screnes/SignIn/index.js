import React, {useEffect} from 'react';
import {Image, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Screen from '../../components/Screen';
import FButton from '../../components/FButton';
import styles from './styles';
import Logo from '../../assets/logo.png';
import {
  loginWithFacebook,
  setToken,
  getUserInfo,
} from '../../store/app/actions';
import {routeNames} from '../../constants';
import AsyncStorage from '@react-native-community/async-storage';

const SignInScreen = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const {
    app: {accessToken},
  } = useSelector(state => state);

  useEffect(() => {
    getToken();
    if (accessToken) {
      navigation.replace(routeNames.home);
    }
  }, [accessToken, navigation]);

  const getToken = async () => {
    const token = await AsyncStorage.getItem('@access_token');
    dispatch(setToken(token));
    dispatch(getUserInfo(token));
  };

  const renderLogo = () => (
    <View style={styles.logoContainer}>
      <Text style={styles.logoText}>welcome to</Text>
      <Text style={styles.logoTextBold}>weather note</Text>
      <Image source={Logo} />
      <Text style={styles.logoText}>explore interesting for you temes</Text>
    </View>
  );

  return (
    <Screen style={styles.container}>
      {renderLogo()}
      <View style={styles.authButtonContainer}>
        <FButton
          title="FB CONNECT"
          onPress={() => dispatch(loginWithFacebook())}
        />
      </View>
    </Screen>
  );
};

export default SignInScreen;
