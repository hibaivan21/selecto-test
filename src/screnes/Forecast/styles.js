import {StyleSheet} from 'react-native';
import {isIphoneX} from 'react-native-iphone-x-helper';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    marginTop: isIphoneX() ? 50 : 20,
    marginBottom: 150,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 15,
    marginRight: 25,
  },
  cityText: {
    color: '#fff',
    fontSize: 20,
  },
  backButton: {
    height: 25,
  },
});
