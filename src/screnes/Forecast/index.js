import React from 'react';
import {Text, View} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {useSelector} from 'react-redux';
import Share from 'react-native-share';

import ImageButton from '../../components/ImageButton';
import Screen from '../../components/Screen';
import WeatherItem from '../../components/WeatherItem';
import {dimensions} from '../../constants';
import styles from './styles';
import SettingIcon from '../../assets/settings.png';
import BackIcon from '../../assets/back_arrow.png';

const ForecastScreen = ({navigation}) => {
  const {forecast, city} = useSelector(state => state.app);
  const onPressBack = () => {
    navigation.goBack();
  };

  const onPressShare = () => {
    const options = {
      title: '',
      message: `Forecast in ${city} for ${forecast[0].date}: day - ${forecast[0].day}, night - ${forecast[0].night}`,
    };

    Share.open(options).catch(err => console.log(err));
  };

  return (
    <Screen style={styles.container}>
      <View style={styles.header}>
        <ImageButton
          onPress={onPressBack}
          style={styles.backButton}
          image={BackIcon}
        />
        <Text style={styles.cityText}>city - '{city}'</Text>
        <ImageButton image={SettingIcon} onPress={onPressShare} />
      </View>
      <Carousel
        data={forecast}
        renderItem={({item}) => <WeatherItem item={item} />}
        sliderWidth={dimensions.DEVICE_WIDTH}
        itemWidth={dimensions.DEVICE_WIDTH - 50}
      />
    </Screen>
  );
};

export default ForecastScreen;
