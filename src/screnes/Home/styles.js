import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  input: {
    height: 50,
  },
  inputText: {
    fontSize: 40,
    textAlign: 'center',
    color: '#fff',
  },
  findButton: {
    backgroundColor: '#fff',
    marginTop: 60,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 25,
    justifyContent: 'flex-end',
    alignSelf: 'flex-end',
    marginRight: 15,
  },
  findButtonText: {
    fontSize: 20,
    fontWeight: '600',
    flexWrap: 'wrap',
  },
});
