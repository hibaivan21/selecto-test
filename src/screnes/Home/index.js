import React, {useEffect} from 'react';
import {Alert, Keyboard, Text, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import SideMenu from 'react-native-side-menu-updated';
import GradientSwitcher from '../../components/GradientSwitcher';
import Input from '../../components/Input';
import Screen from '../../components/Screen';
import {onChangeCityText, searchForecast} from '../../store/app/actions';
import styles from './styles';
import {dimensions, routeNames} from '../../constants';
import HistoryMenu from '../../components/HistoryMenu';

const HomeScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {city, currentGradient, forecast} = useSelector(state => state.app);
  useEffect(() => {
    if (forecast) {
      navigation.navigate(routeNames.forecast);
    }
  }, [forecast]);

  const renderFindButton = () => (
    <TouchableOpacity
      onPress={() => {
        if (city) {
          dispatch(searchForecast(city));
        } else {
          Alert.alert('Enter the city name');
        }
      }}
      style={styles.findButton}>
      <Text style={[styles.findButtonText, {color: currentGradient[0]}]}>
        ✓ Find
      </Text>
    </TouchableOpacity>
  );

  return (
    <SideMenu
      openMenuOffset={dimensions.DEVICE_WIDTH - 40}
      onSliding={Keyboard.dismiss}
      overlayColor={'rgba(0, 0, 0, 0.4)'}
      menu={<HistoryMenu />}>
      <Screen style={styles.container}>
        {renderFindButton()}
        <Input
          value={city}
          style={styles.input}
          textStyle={styles.inputText}
          placeholder="Weather for City"
          onChangeText={val => dispatch(onChangeCityText(val))}
        />
        <GradientSwitcher />
      </Screen>
    </SideMenu>
  );
};

export default HomeScreen;
