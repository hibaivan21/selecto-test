import AsyncStorage from '@react-native-community/async-storage';
import {Alert} from 'react-native';
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import {forecastResultParser} from '../../config/helpers';
import {apiKeys} from '../../constants';
import {types} from './types';

export const loginWithFacebook = () => dispatch => {
  LoginManager.logInWithPermissions(['email']).then(
    result => {
      if (result.isCancelled) {
        Alert.alert('Login cancelled');
      } else {
        AccessToken.getCurrentAccessToken().then(data => {
          const accessToken = data.accessToken.toString();
          dispatch(setToken(accessToken));
          dispatch(getUserInfo(accessToken));
          AsyncStorage.setItem('@access_token', accessToken);
        });
      }
    },
    error => {
      Alert.alert('Login fail with error: ' + error);
    },
  );
};

export const setToken = accessToken => dispatch =>
  dispatch({type: types.SET_ACCESS_TOKEN, accessToken});

export const getUserInfo = token => dispatch => {
  const PROFILE_REQUEST_PARAMS = {
    fields: {
      string: 'id, email, name',
    },
  };
  const profileRequest = new GraphRequest(
    '/me',
    {token, parameters: PROFILE_REQUEST_PARAMS},
    (error, data) => {
      if (error) {
        Alert('Error occured');
      } else {
        dispatch({type: types.FETCH_PROFILE_SUCCESS, data});
      }
    },
  );
  new GraphRequestManager().addRequest(profileRequest).start();
};

export const switchGradient = colors => dispatch => {
  dispatch({
    type: types.SWITCH_GRADIENT,
    colors,
  });
};

export const onChangeCityText = text => dispatch => {
  dispatch({
    type: types.ON_CHANGE_CITY_TEXT,
    text,
  });
};

export const resetForecast = () => dispatch => {
  dispatch({
    type: types.RESET_FORECAST,
  });
};

export const signOut = () => dispatch => {
  dispatch({
    type: types.SIGN_OUT,
  });
  AsyncStorage.removeItem('@access_token');
};

export const searchForecast = city => dispatch => {
  const url = 'https://api.openweathermap.org/data/2.5';
  const requestOptions = {
    method: 'GET',
    redirect: 'follow',
  };

  fetch(
    `${url}/forecast?q=${city}&appid=${apiKeys.OPEN_WEATHER_KEY}&units=metric`,
    requestOptions,
  )
    .then(async response => {
      if (response.ok) {
        const result = await response.json();
        dispatch({
          type: types.FETCH_FORECAST_SUCCESS,
          forecast: forecastResultParser(result.list),
        });
      } else if (response.status === 404) {
        Alert.alert('City not found');
      } else {
        Alert.alert('Something went wrong');
      }
    })
    .catch(error => console.log('error', error));
};
