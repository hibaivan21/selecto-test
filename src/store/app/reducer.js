import {types} from './types';
import {gradients} from '../../constants';

const initState = {
  accessToken: '',
  currentGradient: gradients[0],
  city: '',
  forecast: null,
  history: [],
};

const appReducer = (state = initState, action) => {
  switch (action.type) {
    case types.SET_ACCESS_TOKEN:
      return {
        ...state,
        accessToken: action.accessToken,
      };
    case types.SWITCH_GRADIENT:
      return {
        ...state,
        currentGradient: action.colors,
      };
    case types.ON_CHANGE_CITY_TEXT:
      return {
        ...state,
        city: action.text,
      };
    case types.RESET_FORECAST:
      return {
        ...state,
        forecast: null,
      };
    case types.FETCH_FORECAST_SUCCESS:
      return {
        ...state,
        forecast: action.forecast,
        history: [
          ...state.history,
          {
            city: state.city,
            colors: state.currentGradient,
            date: action.forecast[0].date,
          },
        ],
      };
    default:
      return {
        ...state,
      };
  }
};

export default appReducer;
