import {types as appTypes} from '../app/types';

const initState = {
  email: '',
  name: '',
};

const userReducer = (state = initState, action) => {
  switch (action.type) {
    case appTypes.FETCH_PROFILE_SUCCESS:
      const {data} = action;
      return {
        ...state,
        email: data.email || 'No email provided',
        name: data.name,
      };
    default:
      break;
  }
  return {
    ...state,
  };
};

export default userReducer;
