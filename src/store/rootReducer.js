import {combineReducers} from 'redux';
import user from './user/reducer';
import app from './app/reducer';
import {types} from './app/types';

const reducer = combineReducers({
  user,
  app,
});

const rootReducer = (state, action) => {
  if (action.type === types.SIGN_OUT) {
    return reducer(undefined, action);
  }
  return reducer(state, action);
};

export default rootReducer;
